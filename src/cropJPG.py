import cv2, os, glob

dataset_path = "D:\\images\\source\\rgb"
output_path = "D:\\images\\rgb"

counter = 1
path, dirs, files = next(os.walk(dataset_path))
numfiles = len(files)

for pathAndFilename in glob.iglob(os.path.join(dataset_path, "*.jpg")):  
	title, ext = os.path.splitext(os.path.basename(pathAndFilename))
	print(str(100*counter/numfiles)[:4] + "% - Converting " + title + ext)
	
	img = cv2.imread(dataset_path + "\\" + title + '.jpg')
	crop_img = img[:,308:-308,:]
	cv2.imwrite(output_path + "\\" + title + '.jpg', crop_img)
	counter +=1
