import numpy
import imageio, glob, os, scipy.io as sio

dataset_path = "D:\\images\\source\\depth"
output_path = "D:\\images\\depth"

counter = 1
path, dirs, files = next(os.walk(dataset_path))
numfiles = len(files)

for pathAndFilename in glob.iglob(os.path.join(dataset_path, "*.mat")):  
	title, ext = os.path.splitext(os.path.basename(pathAndFilename))
	print(str(100*counter/numfiles)[:4] + "% - Converting " + title + ext)
	
	mat = sio.loadmat(dataset_path + "\\" + title + '.mat')
	mat = mat["depth"]
	imageio.imsave(output_path + "\\" + title + '.png', mat)
	counter += 1
	