import numpy
import imageio, glob, os, scipy.io as sio

dataset_path = "D:\\images\\rgb"
output_path = "D:\\images\\labels"

counter = 1
path, dirs, files = next(os.walk(dataset_path))
numfiles = len(files)

for pathAndFilename in glob.iglob(os.path.join(dataset_path, "*.jpg")):  
	title, ext = os.path.splitext(os.path.basename(pathAndFilename))
	print(str(100*counter/numfiles)[:4] + "% - Creating " + title + ".txt")
	open(output_path + "\\" + title + '.txt', "a")
	counter += 1
	