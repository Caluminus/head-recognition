from os.path import dirname, join as pjoin
import scipy.io as sio
import numpy as np
from skimage.io import imsave

mat = sio.loadmat("D:\\GitHub\\head-recognition\\data\\office\\data_02-20-19\\depth\\0001.mat")
mat = mat["depth"]

newmat = np.zeros((mat.shape[0], mat.shape[1], 3))
print(mat.shape[0], mat.shape[1])
for i in range(mat.shape[0]):
	for j in range(mat.shape[1]):
		pixel = (mat[i,j]).tobytes()
		#print(pixel)
		newmat[i,j,0] = pixel[0]
		newmat[i,j,1] = pixel[1]
		
imsave("output.jpg", newmat)