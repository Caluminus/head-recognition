# Install script for directory: C:/GitHub/head-recognition/darknet

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/GitHub/head-recognition/darknet")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/lib/darklibd.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/GitHub/head-recognition/darknet/Debug/darklibd.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/lib/darklib.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/GitHub/head-recognition/darknet/Release/darklib.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/lib/darklib.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/GitHub/head-recognition/darknet/MinSizeRel/darklib.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/lib/darklib.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "C:/GitHub/head-recognition/darknet/RelWithDebInfo/darklib.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darklibd.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE SHARED_LIBRARY FILES "C:/GitHub/head-recognition/darknet/Debug/darklibd.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darklib.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE SHARED_LIBRARY FILES "C:/GitHub/head-recognition/darknet/Release/darklib.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darklib.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE SHARED_LIBRARY FILES "C:/GitHub/head-recognition/darknet/MinSizeRel/darklib.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darklib.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE SHARED_LIBRARY FILES "C:/GitHub/head-recognition/darknet/RelWithDebInfo/darklib.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/GitHub/head-recognition/darknet/include/activation_layer.h;C:/GitHub/head-recognition/darknet/include/activations.h;C:/GitHub/head-recognition/darknet/include/avgpool_layer.h;C:/GitHub/head-recognition/darknet/include/batchnorm_layer.h;C:/GitHub/head-recognition/darknet/include/blas.h;C:/GitHub/head-recognition/darknet/include/box.h;C:/GitHub/head-recognition/darknet/include/classifier.h;C:/GitHub/head-recognition/darknet/include/col2im.h;C:/GitHub/head-recognition/darknet/include/connected_layer.h;C:/GitHub/head-recognition/darknet/include/convolutional_layer.h;C:/GitHub/head-recognition/darknet/include/cost_layer.h;C:/GitHub/head-recognition/darknet/include/crnn_layer.h;C:/GitHub/head-recognition/darknet/include/crop_layer.h;C:/GitHub/head-recognition/darknet/include/dark_cuda.h;C:/GitHub/head-recognition/darknet/include/darkunistd.h;C:/GitHub/head-recognition/darknet/include/data.h;C:/GitHub/head-recognition/darknet/include/deconvolutional_layer.h;C:/GitHub/head-recognition/darknet/include/demo.h;C:/GitHub/head-recognition/darknet/include/detection_layer.h;C:/GitHub/head-recognition/darknet/include/dropout_layer.h;C:/GitHub/head-recognition/darknet/include/gemm.h;C:/GitHub/head-recognition/darknet/include/gru_layer.h;C:/GitHub/head-recognition/darknet/include/http_stream.h;C:/GitHub/head-recognition/darknet/include/im2col.h;C:/GitHub/head-recognition/darknet/include/image.h;C:/GitHub/head-recognition/darknet/include/image_opencv.h;C:/GitHub/head-recognition/darknet/include/layer.h;C:/GitHub/head-recognition/darknet/include/list.h;C:/GitHub/head-recognition/darknet/include/local_layer.h;C:/GitHub/head-recognition/darknet/include/lstm_layer.h;C:/GitHub/head-recognition/darknet/include/matrix.h;C:/GitHub/head-recognition/darknet/include/maxpool_layer.h;C:/GitHub/head-recognition/darknet/include/network.h;C:/GitHub/head-recognition/darknet/include/normalization_layer.h;C:/GitHub/head-recognition/darknet/include/option_list.h;C:/GitHub/head-recognition/darknet/include/parser.h;C:/GitHub/head-recognition/darknet/include/region_layer.h;C:/GitHub/head-recognition/darknet/include/reorg_layer.h;C:/GitHub/head-recognition/darknet/include/reorg_old_layer.h;C:/GitHub/head-recognition/darknet/include/rnn_layer.h;C:/GitHub/head-recognition/darknet/include/route_layer.h;C:/GitHub/head-recognition/darknet/include/shortcut_layer.h;C:/GitHub/head-recognition/darknet/include/softmax_layer.h;C:/GitHub/head-recognition/darknet/include/tree.h;C:/GitHub/head-recognition/darknet/include/upsample_layer.h;C:/GitHub/head-recognition/darknet/include/utils.h;C:/GitHub/head-recognition/darknet/include/version.h;C:/GitHub/head-recognition/darknet/include/yolo_layer.h;C:/GitHub/head-recognition/darknet/include/darknet.h;C:/GitHub/head-recognition/darknet/include/yolo_v2_class.hpp")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/include" TYPE FILE FILES
    "C:/GitHub/head-recognition/darknet/src/activation_layer.h"
    "C:/GitHub/head-recognition/darknet/src/activations.h"
    "C:/GitHub/head-recognition/darknet/src/avgpool_layer.h"
    "C:/GitHub/head-recognition/darknet/src/batchnorm_layer.h"
    "C:/GitHub/head-recognition/darknet/src/blas.h"
    "C:/GitHub/head-recognition/darknet/src/box.h"
    "C:/GitHub/head-recognition/darknet/src/classifier.h"
    "C:/GitHub/head-recognition/darknet/src/col2im.h"
    "C:/GitHub/head-recognition/darknet/src/connected_layer.h"
    "C:/GitHub/head-recognition/darknet/src/convolutional_layer.h"
    "C:/GitHub/head-recognition/darknet/src/cost_layer.h"
    "C:/GitHub/head-recognition/darknet/src/crnn_layer.h"
    "C:/GitHub/head-recognition/darknet/src/crop_layer.h"
    "C:/GitHub/head-recognition/darknet/src/dark_cuda.h"
    "C:/GitHub/head-recognition/darknet/src/darkunistd.h"
    "C:/GitHub/head-recognition/darknet/src/data.h"
    "C:/GitHub/head-recognition/darknet/src/deconvolutional_layer.h"
    "C:/GitHub/head-recognition/darknet/src/demo.h"
    "C:/GitHub/head-recognition/darknet/src/detection_layer.h"
    "C:/GitHub/head-recognition/darknet/src/dropout_layer.h"
    "C:/GitHub/head-recognition/darknet/src/gemm.h"
    "C:/GitHub/head-recognition/darknet/src/gru_layer.h"
    "C:/GitHub/head-recognition/darknet/src/http_stream.h"
    "C:/GitHub/head-recognition/darknet/src/im2col.h"
    "C:/GitHub/head-recognition/darknet/src/image.h"
    "C:/GitHub/head-recognition/darknet/src/image_opencv.h"
    "C:/GitHub/head-recognition/darknet/src/layer.h"
    "C:/GitHub/head-recognition/darknet/src/list.h"
    "C:/GitHub/head-recognition/darknet/src/local_layer.h"
    "C:/GitHub/head-recognition/darknet/src/lstm_layer.h"
    "C:/GitHub/head-recognition/darknet/src/matrix.h"
    "C:/GitHub/head-recognition/darknet/src/maxpool_layer.h"
    "C:/GitHub/head-recognition/darknet/src/network.h"
    "C:/GitHub/head-recognition/darknet/src/normalization_layer.h"
    "C:/GitHub/head-recognition/darknet/src/option_list.h"
    "C:/GitHub/head-recognition/darknet/src/parser.h"
    "C:/GitHub/head-recognition/darknet/src/region_layer.h"
    "C:/GitHub/head-recognition/darknet/src/reorg_layer.h"
    "C:/GitHub/head-recognition/darknet/src/reorg_old_layer.h"
    "C:/GitHub/head-recognition/darknet/src/rnn_layer.h"
    "C:/GitHub/head-recognition/darknet/src/route_layer.h"
    "C:/GitHub/head-recognition/darknet/src/shortcut_layer.h"
    "C:/GitHub/head-recognition/darknet/src/softmax_layer.h"
    "C:/GitHub/head-recognition/darknet/src/tree.h"
    "C:/GitHub/head-recognition/darknet/src/upsample_layer.h"
    "C:/GitHub/head-recognition/darknet/src/utils.h"
    "C:/GitHub/head-recognition/darknet/src/version.h"
    "C:/GitHub/head-recognition/darknet/src/yolo_layer.h"
    "C:/GitHub/head-recognition/darknet/include/darknet.h"
    "C:/GitHub/head-recognition/darknet/include/yolo_v2_class.hpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/uselib.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/Debug/uselib.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/uselib.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/Release/uselib.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/uselib.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/MinSizeRel/uselib.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/uselib.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/RelWithDebInfo/uselib.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darknet.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/Debug/darknet.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darknet.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/Release/darknet.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darknet.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/MinSizeRel/darknet.exe")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/darknet.exe")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet" TYPE EXECUTABLE FILES "C:/GitHub/head-recognition/darknet/RelWithDebInfo/darknet.exe")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets.cmake"
         "C:/GitHub/head-recognition/darknet/CMakeFiles/Export/C_/GitHub/head-recognition/darknet/share/darknet/DarknetTargets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/share/darknet" TYPE FILE FILES "C:/GitHub/head-recognition/darknet/CMakeFiles/Export/C_/GitHub/head-recognition/darknet/share/darknet/DarknetTargets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-debug.cmake")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/share/darknet" TYPE FILE FILES "C:/GitHub/head-recognition/darknet/CMakeFiles/Export/C_/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-debug.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-minsizerel.cmake")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/share/darknet" TYPE FILE FILES "C:/GitHub/head-recognition/darknet/CMakeFiles/Export/C_/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-minsizerel.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-relwithdebinfo.cmake")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/share/darknet" TYPE FILE FILES "C:/GitHub/head-recognition/darknet/CMakeFiles/Export/C_/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-relwithdebinfo.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "C:/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-release.cmake")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/share/darknet" TYPE FILE FILES "C:/GitHub/head-recognition/darknet/CMakeFiles/Export/C_/GitHub/head-recognition/darknet/share/darknet/DarknetTargets-release.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/GitHub/head-recognition/darknet/share/darknet/DarknetConfig.cmake;C:/GitHub/head-recognition/darknet/share/darknet/DarknetConfigVersion.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/GitHub/head-recognition/darknet/share/darknet" TYPE FILE FILES
    "C:/GitHub/head-recognition/darknet/CMakeFiles/DarknetConfig.cmake"
    "C:/GitHub/head-recognition/darknet/DarknetConfigVersion.cmake"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "C:/GitHub/head-recognition/darknet/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
